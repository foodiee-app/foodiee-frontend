module.exports = {
  mode: 'jit',
  content: [
    './public/**/*.html',
  './src/**/*.{js,jsx,ts,tsx,vue}'
  ],
  theme: {
    colors: {
      'foodiee-black-primary': '#131515',
      'foodiee-black-secondary': '#2B2C28',
      'foodiee-white': '#FFFAFB',
      'foodiee-button-primary': '#7DE2D1',
      'foodiee-button-secondary': '#339989',
    },
    fontFamily: {
      foodiee_logo: ["foodiee-logo", "foodiee-logo"],
    },
    extend: {},
  },
  plugins: [],
}
