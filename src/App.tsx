import React from 'react'
import {
    BrowserRouter as Router,
    Routes,
    Route,
    BrowserRouter
} from 'react-router-dom'
import ChooseProfile from './routes/Profiles'
import Index from './routes/Index';
import Home from './routes/Home';
import RecipePage from './routes/RecipePage';
import ShoppingListPage from './routes/ShoppingListPage';
import RecipeCreation from './routes/RecipeCreation';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Index />}/>
                <Route path='/home' element={<Home />}/>
                <Route path='/profiles' element={<ChooseProfile />}/>
                <Route path='/recipe' element={<RecipePage />}/>
                <Route path='/shopping-list' element={<ShoppingListPage />}/>
                <Route path='/recipe/create' element={<RecipeCreation />}/>
            </Routes>
        </BrowserRouter>
    )
}

export default App