import React, { ReactNode, useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import AddToListButton from "../components/AddToListButton";
import Break from "../components/Break";
import Header from "../components/Header";
import { Recipe } from "../models/Recipe";
import { User } from "../models/User";
import { fetchRecipe } from "../service/RecipeService";
import { fetchUser } from "../service/UserService";


function RecipePage() {
    
    const [searchParams, setSearchParams] = useSearchParams();    
    const [recept, setRecept] = useState<Recipe | null>(null);
    const [receptAuthor, setReceptAuthor] = useState<string | null>(null);
    const [receptIngriedients, setReceptIngriedients] = useState<ReactNode[] | null>();

    useEffect(() => {
        var queryId: string | null = searchParams.get("id");
        
        if(queryId === null) {
            return;
        }

        fetchRecipe(parseInt(searchParams.get("id")!)).then((response : null | Recipe) => {
            if(!response) {
                return;
            }
            setRecept(response);

            const ingriedients : ReactNode[] = []
            for(var ingriedient of response.ingredients) {
                ingriedients.push(
                    <li key={ingriedient.id}>{ingriedient.amount}{ingriedient.unit} {ingriedient.name}</li>
                );
            }
            setReceptIngriedients(ingriedients);
        })
    }, [])

    useEffect(() => {
        if(!recept) {
            return;
        }

        fetchUser(recept.userId).then((response : User | null) => {
            if (!response) {
                return;
            }
            setReceptAuthor(response.name);
        })
    })

    if(!recept) {
        return(
            <Header/>
        )
    }

    return(
        <div>
            <Header extraIcons="true" />
            <div className=" grid md:justify-items-center">
                <div className="text-2xl mt-[5px] mx-[15px]">{recept?.name}</div>
                <div className="flex ml-[20px] mt-[5px] justify-start gap-[10px] text-xs">
                    <div className="flex">
                        <img src="images/icons/clock_white.png" className="h-[14px] inline"/>
                        <div className="ml-[5px]">{recept?.preparationTime} min.</div>
                    </div>
                    {
                        recept?.isVeggie &&
                        <div className="flex">
                            <img src="images/icons/leaf_white.png" className="h-[14px]" />
                            <div className="ml-[5px]">Veggie</div>
                        </div>
                    }
                    <div className="flex">
                        <img src="images/icons/user_white.png" className="h-[14px]" />
                        <div className="ml-[5px]">{receptAuthor}</div>
                    </div>
                </div>
                <div className="absolute right-[45px] top-[65px]"><AddToListButton {...recept}/></div>
                <img src={recept.thumbnailPath} className="h-[208px] w-[375px] object-cover mt-[10px]"/>
            </div>
            <Break />
            <div className="mx-[10px]">
                Zutaten
                <div className="text-xs ml-[4px]">
                    {receptIngriedients}
                </div>
            </div>
            <Break />
            <div className="mx-[10px]">
                Zubereitung
                <div className="text-xs my-[10px] whitespace-pre-wrap">
                    {recept.preparation}
                </div>
            </div>
        </div>
    )
}

export default RecipePage;