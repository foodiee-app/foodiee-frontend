import React, { ReactNode, useEffect, useState } from "react";
import Header from "../components/Header";
import RecipePreview from "../components/RecipePreview";
import { Recipe } from "../models/Recipe";
import { fetchAllRecipes } from "../service/RecipeService";

function Home() {
    const [recipes, setRecipes] = useState<ReactNode | null>(null);

    useEffect(() => {
        fetchAllRecipes().then((response : Array<Recipe>) => {
            const recipesPreviews: ReactNode[] = [];

            for(var recipe of response) {
                recipesPreviews.push(<RecipePreview key={recipe.id} {...recipe}/>)
            }

            setRecipes(recipesPreviews);
        })
    }, [])

    return (
        <div>
            <Header extraIcons='true'/>
            <div className="mt-[15px] mx-[20px] grid  md:grid-flow-col md:auto-cols-max md:gap-x-[50px] justify-items-center">
            {recipes}
            </div>
            <div className="fixed bottom-[10px] right-[10px] md:hover:cursor-pointer z-[100]" onClick={() => window.location.href = '/recipe/create'}>
                <img src="images/icons/plus.png" className="absolute h-[32px] ml-[16px] mt-[16px]" />
                <div className="h-[64px] w-[64px] bg-foodiee-button-primary rounded-[100px] shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)]"/>
            </div>
        </div>
    );
}

export default Home;