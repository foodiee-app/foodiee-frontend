import React, { ReactNode, useEffect, useState } from "react";
import Header from "../components/Header";
import AvatarSelection from "../components/AvatarSelection";
import { User } from "../models/User";
import { fetchAllUsers } from "../service/UserService";

function Profiles() {
    const [users, setUsers] = useState<ReactNode | null>(null);

    useEffect(() => {
        fetchAllUsers().then((response : Array<User>) => { 
            const avatars : any = [];
            
            for(var user of response) {
                avatars.push(<AvatarSelection key={user.id} userId={user.id} />)
            }

            setUsers(avatars)
        });
    }, [])

    return (
        <div>
            <Header />
            <div className='grid justify-items-center'>
                <div className='mt-[77px] mb-[29px] text-3xl md:text-5xl'>Wer stöbert gerade?</div>
                <div className='grid grid-cols-2 md:grid-cols-4 gap-[66px] md:mt-[10vh]'>
                    {users}
                </div>
            </div>
        </div>
    );
}

export default Profiles