import React, { ReactNode, useEffect, useState } from "react";
import Header from "../components/Header";
import { ShoppingListEntry } from "../models/ShoppingListEntry";
import { clear, fetchShoppingList } from "../service/ShoppingListService"

function ShoppingListPage() {
    const [shoppingListEntries, setShoppingListEntries] = useState<null | ReactNode>(null);
    useEffect(() => {
        fetchShoppingList().then((entries: ShoppingListEntry[]) => {
            var entryReactNodes : ReactNode[] = [];  
            
            for(var entry of entries) {
                entryReactNodes.push(<li key={entry.ingredientId}>{entry.totalAmount}{entry.unit} {entry.name}</li>)
            }

            setShoppingListEntries(entryReactNodes);
        })
    }, [])

    return(
        <div>
            <Header />
            <div className="ml-[20px]">
                <div className="text-3xl mt-[10px]">Einkaufsliste</div>
                <div className="grid justify-items-center items-center text-xs w-[100px] h-[23px] bg-foodiee-button-secondary rounded-[15px] mt-[10px] hover:cursor-pointer" 
                onClick={() => {clear(); window.location.reload();}}>
                    Clear
                </div>
                <div className="my-[10px]">Zu kaufen sind:</div>
                <div className="ml-[5px] text-xs">
                    {shoppingListEntries}
                </div>
            </div>
        </div>
    )
}

export default ShoppingListPage;