import React, { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { User } from "../models/User";
import { fetchUser } from "../service/UserService";
import { getCookieValue } from "../util/Utility";

function Index() {
    const [user, setUser] = useState<User | null>(null);
    let userId : string | undefined = getCookieValue('userId');

    useEffect(() => {
        if(userId == undefined) return;

        fetchUser(parseInt(userId)).then((response : User | null) => {
            if(response == null) {
                return;
            }
            setUser(response);
        })
    }, []);

    if(userId == undefined) {
        return userSelection();
    }

    return (<Navigate to={'/home'}/>);
}

function userSelection() {
    return (<Navigate to={'/profiles'}/>);
}

export default Index;