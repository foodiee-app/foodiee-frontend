import React, { FormEvent, ReactNode, useEffect, useState } from "react";
import Break from "../components/Break";
import Header from "../components/Header";
import autosize from 'autosize';
import { createRecipe, uploadFile } from "../service/RecipeService";
import { Recipe } from "../models/Recipe";
import { Ingredient } from "../models/Ingredient";
import { getCookieValue } from "../util/Utility";


function RecipeCreation() { 
    const [thumbnail, setThumbnail] = useState<string>("/images/blank.jpg");
    const [veggie, setVeggie] = useState<boolean>(false);
    const [name, setName] = useState<undefined | string>(undefined);
    const [preparation, setPreparation] = useState<undefined | string>(undefined);
    const [preparationTime, setPreparationTime] = useState<number>(0);
    const [ingredients, setIngredients] = useState<Array<Ingredient>>([]);

    const [ingredientElements, setIngredientElements] = useState<Array<ReactNode>>([]);

    useEffect(() => {
        autosize(document.querySelectorAll('textarea'));
    })

    function handleFileChange(event : any) {
        var file = event.target.files[0];
        const formData : FormData = new FormData();
        formData.append('file', file);
        uploadFile(formData).then((response : string) => setThumbnail(response));
    }
 
    function saveRecipe() {
        const recipe : Recipe = {
            id: 0,
            ingredients: ingredients,
            isVeggie: veggie,
            name: name!,
            preparation: preparation!,
            preparationTime: preparationTime,
            thumbnailPath: thumbnail,
            userId: parseInt(getCookieValue('userId')!)
        }
        if(!recipe.preparation || !recipe.name) {
            return;
        }

        createRecipe(recipe);
        window.location.href="/home"
    }

    function handleIngredientSubmit(event : any) {
        event.preventDefault();

        const ingredient : Ingredient = {
            id: ingredients.length,
            name: event.target.name.value,
            amount: event.target.amount.value,
            unit: event.target.unit.value
        }

        if(!ingredient.amount || !ingredient.name || !ingredient.unit) {
            return;
        }
        
        const elements : Ingredient[] = ingredients;
        elements.push(ingredient);
        setIngredients(elements);

        const nodeElements : ReactNode[] = [...ingredientElements];
        nodeElements.push(<li key={ingredient.id}>{ingredient.amount}{ingredient.unit} {ingredient.name}</li>);
        setIngredientElements(nodeElements);

        event.target.name.value = "";
        event.target.amount.value = "";
        event.target.unit.value = "";
    }

    return (
        <div>
            <Header />
            <div className="text-3xl mx-[5px] mt-[25px]">Vorschaubild</div>
            <div className="bg-foodiee-button-secondary h-[23px] w-[100px] absolute rounded-[15px] pl-[20px] pt-[4px] top-[86px] left-[256px] text-xs shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)]" onClick={saveRecipe}>Speichern</div>
            <div className="w-[375px] h-[208px] bg-cover bg-center" style={{backgroundImage : `url(${thumbnail})`}}>
                <div className="w-[64px] h-[64px] ml-[300px] mt-[130px] absolute overflow-hidden bg-foodiee-white rounded-[15px]">
                    <input type="file" className="w-[64px] h-[64px] absolute opacity-0 z-40" onChange={handleFileChange}/>
                    <img src="/images/icons/file.png" className="w-[48px] m-[8px] absolute"/>
                </div>
            </div>
            <Break />
            <div className="mx-[10px]">
                <div className="text-2xl"><input maxLength={15} onChange={(e) => setName(e.target.value)} id="title" className="bg-foodiee-black-secondary w-[90vw]" placeholder="Titel..." /></div>
                <div className="text-xs">
                    Dauer [min.]:
                    <input className="bg-foodiee-black-secondary w-[30px] ml-[3px] border-b rounded-none" maxLength={3} onChange={(e) => { if(!/^[0-9]{1,3}$/.test(e.target.value)) e.target.value = ""; setPreparationTime(parseInt(e.target.value)) }}/>
                </div>
                <div className="text-xs flex">
                    Veggie:
                    <input className="ml-[3px]" type="checkbox" value="Veggie" onChange={(e) => setVeggie(e.target.checked)}/>
                </div>
            </div>
            <Break />
            <div className="mx-[10px]">
                <div className="text-2xl">Zutaten</div>
                <div className="text-xs ml-[4px]">
                        {ingredientElements}
                </div>
                <form className="mt-[15px]" onSubmit={handleIngredientSubmit}>
                <div className="text-xs">
                        <input name="name" className="bg-foodiee-black-secondary w-[120px] ml-[3px] border-b rounded-none" maxLength={10} placeholder="z.B. Teig" onChange={(e) => { if(!/^[a-zA-Z]{1,10}$/.test(e.target.value)) e.target.value = ""; }}/>
                        <input name="amount" className="bg-foodiee-black-secondary w-[30px] ml-[3px] border-b rounded-none" maxLength={3} placeholder="500" onChange={(e) => { if(!/^[0-9]{1,3}$/.test(e.target.value)) e.target.value = ""; }}/>
                        <input name="unit" className="bg-foodiee-black-secondary w-[30px] ml-[3px] border-b rounded-none" maxLength={2} placeholder="g" onChange={(e) => { if(!/^[a-zA-Z]{1,2}$/.test(e.target.value)) e.target.value = ""; }}/>
                    </div>
                    <button type="submit" className="w-[100px] h-[23px] bg-foodiee-button-secondary text-xs rounded-[15px] mt-[10px]">
                        Hinzufügen
                    </button>
                </form>
            </div>
            <Break />
            <div className="mx-[10px]">
                <div className="text-2xl">Zubereitung</div>
                <div className="text-xs"><textarea onChange={(e) => setPreparation(e.target.value)} id="preparation" className="bg-foodiee-black-secondary w-[90vw]" placeholder="Die Zubereitung erklären..." /></div>
            </div>
        </div>
    )
}

export default RecipeCreation;
