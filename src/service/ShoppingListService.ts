import { Recipe } from "../models/Recipe";
import { ShoppingListEntry } from "../models/ShoppingListEntry";

const API_PATH: string = "https://api.foodiee.de";

export function addReceptIngredients(recept: Recipe): void {
    fetch(API_PATH + "/shopping-list", {
        method: "POST",
        headers: {'Content-Type' : 'application/json'},
        body: JSON.stringify(recept)
    })
}

export async function fetchShoppingList(): Promise<ShoppingListEntry[]> {
    const response = await fetch(API_PATH + '/shopping-list');

    if(response.status != 200) {
        return new Array();
    }

    const data = await response.json();
    const entries: ShoppingListEntry[] = [];

    data.forEach((element : ShoppingListEntry) => {
        entries.push(element)
    })

    return entries;
}

export function clear(): void {
    fetch(API_PATH + "/shopping-list/clear", {
        method: "POST",
        headers: {"Content-Type" : "application/json"}
    });
}