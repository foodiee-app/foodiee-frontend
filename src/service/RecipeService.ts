import { Recipe } from "../models/Recipe";


const API_PATH : string = 'https://api.foodiee.de';

export async function fetchAllRecipes() : Promise<Recipe[]> {
    const response = await fetch(API_PATH + '/recipes');

    if(response.status != 200) {
        return new Array();
    }

    const data = await response.json();
    const recepts: Recipe[] = [];

    data.forEach((element : Recipe) => {
        recepts.push(element)
    })

    return recepts;
}

export async function fetchRecipe(id: number) : Promise<Recipe | null> {
    const response = await fetch(API_PATH + '/recipes/' + id);

    if(response.status != 200) {
        return null;
    }

    const data = await response.json();
    const recipe: Recipe = data;
    
    return recipe;
}

export async function uploadFile(formData: FormData) : Promise<string> {
    const response =  await fetch(API_PATH + '/recipes/upload', {
        method: 'post',
        body: formData
    });
    
    if(response.status != 200) {
        return "";
    }

    return response.text();
}

export function createRecipe(recipe : Recipe) {
    fetch(API_PATH + '/recipes/create', {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(recipe)
    })
}