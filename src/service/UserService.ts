import {User} from "../models/User";


const API_PATH : string = 'https://api.foodiee.de';

export async function fetchAllUsers() : Promise<User[]> {
    const response = await fetch(API_PATH + '/users');
    
    if(response.status != 200) {
        return new Array();
    }
    
    const data = await response.json();
    const users = new Set<User>();

    data.forEach((element : any) => {
        users.add(element)
    });

    return Array.from(users);
}

export async function fetchUser(id: number) : Promise<User | null> {
    const response = await fetch(API_PATH + '/users/' + id);

    if(response.status != 200) {
        return null;
    }

    const data = await response.json();
    const user : User = data;

    return user;
}