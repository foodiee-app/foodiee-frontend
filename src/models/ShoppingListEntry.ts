import { Ingredient } from "./Ingredient";

export interface ShoppingListEntry {
    ingredientId: number;
    name: string;
    totalAmount: number;
    unit: string;
}