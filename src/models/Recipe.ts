import { Ingredient } from "./Ingredient";

export interface Recipe {
    id: number;
    userId: number;
    name: string;
    thumbnailPath: string;
    isVeggie: boolean;
    preparationTime: number;
    ingredients: Ingredient[];
    preparation: string;
}