
export interface User {
    id: number;
    name: string;
    picturePath: string;
}