import React from 'react'

function Break() {
    return (
        <div className="w-[100vw] h-[1px] bg-foodiee-white opacity-50 my-[20px]" />
    )
}

export default Break;