import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { Recipe } from "../models/Recipe";
import { User } from "../models/User";
import { addReceptIngredients } from "../service/ShoppingListService";
import { fetchUser } from "../service/UserService";
import AddButton from "./AddToListButton";

function RecipePreview(recipe : Recipe) {

    const [recipeAuthor, setRecipeAuthor] = useState<string | null>(null); 

    useEffect(() => {
        fetchUser(recipe.userId).then((user: User  | null) => {
            var userName: string = "null";
            
            if(user !== null) {
                userName = user.name;
            }

            setRecipeAuthor(userName);
        });
    }, [])

    return(
        <div className="bg-foodiee-white w-[329px] h-[247px] rounded-[25px] mb-[35px] text-foodiee-black-primary md:hover:cursor-pointer">
            <AddButton {...recipe} />
            <div onClick={() => window.location.href="/recipe?id=" + recipe.id}>
                <img src={recipe.thumbnailPath} className="w-[290px] h-[153px] object-cover rounded-[15px] mt-[20px] ml-[20px]" />
                <div className="text-2xl ml-[20px] mt-[10px] w-[284px] h-[25px]">{recipe.name}</div>
                <div className="flex ml-[20px] mt-[5px] justify-start gap-[10px] text-xs">
                    <div className="flex">
                        <img src="images/icons/clock.png" className="h-[14px] inline"/>
                        <div className="ml-[5px]">{recipe.preparationTime} min.</div>
                    </div>
                    {
                        recipe.isVeggie &&
                        <div className="flex">
                            <img src="images/icons/leaf.png" className="h-[14px]" />
                            <div className="ml-[5px]">Veggie</div>
                        </div>
                    }
                    <div className="flex">
                        <img src="images/icons/user.png" className="h-[14px]" />
                        <div className="ml-[5px]">{recipeAuthor}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default RecipePreview;