import React from 'react'
import { deleteCookie } from '../util/Utility';

function Header(props : any) {
    return (
        <div className='bg-foodiee-black-primary text-foodiee-white grid grid-cols-2 items-center h-14 px-4 sticky top-0 z-[100]'>
            <div className='font-foodiee_logo text-3xl hover:cursor-pointer' onClick={() => window.location.href='/'}>Foodiee</div>
            {
                props.extraIcons == 'true' &&
                <div className='justify-self-end grid grid-cols-2 gap-[7px]'>
                    <img src="images/icons/shopping_list.png" className='w-[32px]' onClick={() => window.location.href='/shopping-list'} />
                    <img src="images/icons/user_white.png" className='w-[32px]' onClick={() => {window.location.href='/profiles'; deleteCookie('userId')}} />
                </div>
            }
        </div>
    )
}

export default Header;