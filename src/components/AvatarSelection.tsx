import React, { useEffect, useState } from "react";
import { User } from "../models/User";
import { fetchUser } from "../service/UserService";

function AvatarSelection(props: any) {
    const [user, setUser] = useState<User | null>(null);

    useEffect(() => {
        fetchUser(props.userId).then((response : User | null) => {
            if (response == null) {
                return;
            }
            setUser(response);
        });
    }, []);

    if(!user) {
        return(
            <div className="grid justify-items-center items-center h-[92px] w-[92px] md:h-[128px] md:w-[128px]">
                Loading...
            </div>
        );
    }

    return (
        <div className="grid justify-items-center hover:md:opacity-50 hover:md:cursor-pointer" onClick={() => {document.cookie="userId=" + user.id; window.location.href='/'}}>
            <img src={user.picturePath} className="object-cover h-[92px] w-[92px] md:h-[128px] md:w-[128px] rounded-[15px]"></img>
            <span className="mt-[5px] md:text-xl">{user.name}</span>
        </div>
    )
}

export default AvatarSelection;