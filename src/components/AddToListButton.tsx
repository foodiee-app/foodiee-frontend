import React from "react";
import { Recipe } from "../models/Recipe";
import { addReceptIngredients } from "../service/ShoppingListService";


function AddToListButton(recipe: Recipe) {
    return(
        <div className="ml-[290px] mt-[10px]" onClick={() => addReceptIngredients(recipe)}>
            <img src="images/icons/plus.png" className="absolute h-[16px] z-40 ml-[8px] mt-[8px]" />
            <div className="absolute h-[32px] w-[32px] bg-foodiee-button-secondary rounded-[100px] shadow-[0_4px_4px_0px_rgba(0,0,0,0.25)]"/>
        </div>
    );
}

export default AddToListButton;