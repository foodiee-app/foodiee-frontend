export function getCookieValue(name: string) {
    var value : string | undefined = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop();
    return value;
}

export function deleteCookie(name: string) {
    document.cookie = name + '=; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/';
}